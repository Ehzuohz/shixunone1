### git 笔记

#### 回滚文件状态

1. git reset --hard HEAD^^ /commitId 直接回滚到 commitId 之后所有记录 不显示
2. git revert --no-commit

  的所有 commit 都将被清除;

# 而 revert 仅是撤销指定 commit 的修改,并不影响后续的 commit。

# 2.reset 执行后不会产生记录,revert 执行后会产生记录。
